<?php

namespace Setdroy\MaterialsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Material
 *
 * @ORM\Table(name="material")
 * @ORM\Entity(repositoryClass="Setdroy\MaterialsBundle\Repository\MaterialRepository")
 */
class Material
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, unique=true)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Setdroy\MaterialsBundle\Entity\MaterialGroup")
     * @ORM\JoinColumn(name="material_group", referencedColumnName="id")
     */
    protected $materialGroup;

    /**
     * @ORM\ManyToOne(targetEntity="Setdroy\MaterialsBundle\Entity\MeasureUnit")
     * @ORM\JoinColumn(name="measure_unit", referencedColumnName="id")
     */
    protected $measureUnit;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Material
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Material
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set material group
     *
     * @param MaterialGroup $materialGroup
     *
     * @return Material
     */
    public function setMaterialGroup(MaterialGroup $materialGroup)
    {
        $this->materialGroup = $materialGroup;

        return $this;
    }

    /**
     * Get material group
     *
     * @return MaterialGroup
     */
    public function getMaterialGroup()
    {
        return $this->materialGroup;
    }

    /**
     * Set measure unit
     *
     * @param MeasureUnit $measureUnit
     *
     * @return Material
     */
    public function setMeasureUnit(MeasureUnit $measureUnit)
    {
        $this->measureUnit = $measureUnit;

        return $this;
    }

    /**
     * Get measure unit
     *
     * @return MeasureUnit
     */
    public function getMeasureUnit()
    {
        return $this->measureUnit;
    }
}

