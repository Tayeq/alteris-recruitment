<?php

namespace Setdroy\MaterialsBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Setdroy\MaterialsBundle\Entity\Material;
use Setdroy\MaterialsBundle\Entity\MaterialGroup;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Setdroy\MaterialsBundle\Entity\MeasureUnit;
use FOS\RestBundle\Request\ParamFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Setdroy\MaterialsBundle\Form\Type\MaterialType;

class ApiController extends FOSRestController
{

    public function getMaterialsAction()
    {
        $material = $this->getDoctrine()
            ->getRepository(Material::class)
            ->findAllParsed();

        $view = $this->view($material, 200);
        return $this->handleView($view);
    }

    public function getMaterialAction($id)
    {
        $material = $this->getDoctrine()
            ->getRepository(Material::class)
            ->findParsed($id);

        if ($material) {
            $view = $this->view($material, 200);
            return $this->handleView($view);
        }

        $view = $this->view(array('error' => 'Material with id ' . $id . ' not found.'), 404);
        return $this->handleView($view);

    }

    public function postMaterialsAction(Request $request)
    {
        $return = array();
        $material = new Material();

        if ($id = $request->request->get('id')) {
            $material = $this->getDoctrine()
                ->getRepository(Material::class)
                ->find($id);
        }

        if (!$material) {
            $return['errors'][] = 'Material with ID = ' . $id . ' not found';
            $view = $this->view($return, 400);
            return $this->handleView($view);
        }

        foreach ($this->prepareRequest($request->request->all(), 'material') as $key => $value) {
            switch ($key) {
                case 'materialGroup':
                    $materialGroup = $this->getDoctrine()
                        ->getRepository(MaterialGroup::class)
                        ->find($value);
                    if ($materialGroup) {
                        $material->setMaterialGroup($materialGroup);
                    }
                    break;
                case 'measureUnit':
                    $measureUnit = $this->getDoctrine()
                        ->getRepository(MeasureUnit::class)
                        ->find($value);
                    if ($measureUnit) {
                        $material->setMeasureUnit($measureUnit);
                    }
                    break;
                case 'id':
                    break;
                default:
                    $material->{'set' . ucfirst($key)}($value);
                    break;
            }

        }
        $validator = $this->get('validator');
        $errors = $validator->validate($material);

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $return['errors'][] = $error->getPropertyPath() . ': ' . $error->getMessage();
            }

            $view = $this->view($return, 400);
            return $this->handleView($view);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($material);
        $em->flush();

        $materialParsed = $this->getDoctrine()
            ->getRepository(Material::class)
            ->findParsed($material->getId());

        $view = $this->view($materialParsed, 200);
        return $this->handleView($view);

    }

    public function getGroupsAction()
    {
        $materialGroups = $this->getDoctrine()
            ->getRepository(MaterialGroup::class)
            ->findAllParsed();

        $view = $this->view($materialGroups, 200);
        return $this->handleView($view);
    }

    public function getGroupAction($id)
    {
        $materialGroup = $this->getDoctrine()
            ->getRepository(MaterialGroup::class)
            ->findParsed($id);


        if ($materialGroup) {
            $view = $this->view($materialGroup, 200);
            return $this->handleView($view);
        }

        $view = $this->view(array('error' => 'Material group with id: ' . $id . ' not found.'), 404);
        return $this->handleView($view);
    }

    public function postGroupsAction(Request $request)
    {
        $return = array();
        $materialGroup = new MaterialGroup();

        if ($id = $request->request->get('id')) {
            $materialGroup = $this->getDoctrine()
                ->getRepository(MaterialGroup::class)
                ->find($id);
        }

        if (!$materialGroup) {
            $return['errors'][] = 'Material Group with id:  ' . $id . ' not found';
            $view = $this->view($return, 400);
            return $this->handleView($view);
        }

        foreach ($this->prepareRequest($request->request->all(), 'material_group') as $key => $value) {
            switch ($key) {
                case 'parent':
                    $parent = $this->getDoctrine()
                        ->getRepository(MaterialGroup::class)
                        ->find($value);
                    if ($parent) {
                        $materialGroup->setParent($parent);
                    } else {
                        $materialGroup->removeParent();
                    }
                    break;
                case 'id':
                    break;
                default:
                    $materialGroup->{'set' . ucfirst($key)}($value);
                    break;
            }

        }
        $validator = $this->get('validator');
        $errors = $validator->validate($materialGroup);

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $return['errors'][] = $error->getPropertyPath() . ': ' . $error->getMessage();
            }

            $view = $this->view($return, 400);
            return $this->handleView($view);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($materialGroup);
        $em->flush();

        $materialGroupParsed = $this->getDoctrine()
            ->getRepository(MaterialGroup::class)
            ->findParsed($materialGroup->getId());

        $view = $this->view($materialGroupParsed, 200);
        return $this->handleView($view);

    }

    public function getMeasureunitsAction()
    {
        $materialGroups = $this->getDoctrine()
            ->getRepository(MeasureUnit::class)
            ->findAll();

        $view = $this->view($materialGroups, 200);
        return $this->handleView($view);
    }

    public function getMeasureunitAction($id)
    {
        $measureUnit = $this->getDoctrine()
            ->getRepository(MeasureUnit::class)
            ->find($id);


        if ($measureUnit) {
            $view = $this->view($measureUnit, 200);
            return $this->handleView($view);
        }

        $view = $this->view(array('error' => 'Measure unit with id: ' . $id . ' not found.'), 404);
        return $this->handleView($view);
    }

    public function postMeasureunitsAction(Request $request)
    {
        $return = array();
        $measureUnit = new MeasureUnit();

        if ($id = $request->request->get('id')) {
            $measureUnit = $this->getDoctrine()
                ->getRepository(MeasureUnit::class)
                ->find($id);
        }

        if (!$measureUnit) {
            $return['errors'][] = 'Measure Unit with ID = ' . $id . ' not found';
            $view = $this->view($return, 400);
            return $this->handleView($view);
        }

        foreach ($this->prepareRequest($request->request->all(), 'measure_unit') as $key => $value) {
            switch ($key) {
                case 'id':
                    break;
                default:
                    $measureUnit->{'set' . ucfirst($key)}($value);
                    break;
            }

        }
        $validator = $this->get('validator');
        $errors = $validator->validate($measureUnit);

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $return['errors'][] = $error->getPropertyPath() . ': ' . $error->getMessage();
            }

            $view = $this->view($return, 400);
            return $this->handleView($view);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($measureUnit);
        $em->flush();

        $view = $this->view($measureUnit, 200);
        return $this->handleView($view);
    }

    private function prepareRequest(array $request, $type = 'material')
    {
        $em = $this->getDoctrine()->getManager();
        $schemaManager = $em->getConnection()->getSchemaManager();
        $columns = $schemaManager->listTableColumns($type);

        $allowedParameters = array();
        foreach ($columns as $column) {
            $allowedParameters[] = $this->dashesToCamelCase($column->getName());
        }

        $return = array();

        foreach ($request as $key => $value) {
            if (in_array($key, $allowedParameters)) {
                $return[$key] = $value;
            }
        }

        return $return;
    }

    private function dashesToCamelCase($string, $capitalizeFirstCharacter = false)
    {
        $str = str_replace(' ', '', ucwords(str_replace(array('-', '_'), ' ', $string)));

        if (!$capitalizeFirstCharacter) {
            $str[0] = strtolower($str[0]);
        }
        return $str;
    }
}
