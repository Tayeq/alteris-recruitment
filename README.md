# Materials API

## Available API requests:

### Materials
- get_materials | GET

~~~
/api/materials.{_format(json|xml)}
~~~
---
- get_material | GET

~~~
/api/materials/{id}.{_format(json|xml)}
~~~
---
- post_materials | POST

~~~
/api/materials.{_format(json|xml)}
~~~
Available request body keys:

- id (if isset will update material, if not will create new)
- code
- name
- materialGroup
- measureUnit

---

### Material Groups
-  get_groups | GET

~~~
/api/groups.{_format(json|xml)}
~~~

---
- get_group | GET

~~~
/api/groups/{id}.{_format(json|xml)}
~~~
---
- post_groups | POST

~~~
/api/groups.{_format(json|xml)}
~~~
Available request body keys:

- id (if isset will update material group, if not will create new)
- name
- parent

---

### Measure Units
-  get_measureunits | GET

~~~
/api/measureunits.{_format(json|xml)}
~~~
---
- get_measureunit | GET

~~~
/api/measureunits/{id}.{_format(json|xml)}
~~~
---
- post_measureunits | POST

~~~
/api/measureunits.{_format(json|xml)}
~~~
Available request body keys:

- id (if isset will update measure unit, if not will create new)
- name
- shortName

---