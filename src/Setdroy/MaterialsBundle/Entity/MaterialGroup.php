<?php

namespace Setdroy\MaterialsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * MaterialGroup
 *
 * @ORM\Table(name="material_group")
 * @ORM\Entity(repositoryClass="Setdroy\MaterialsBundle\Repository\MaterialGroupRepository")
 */
class MaterialGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="MaterialGroup", mappedBy="parent")
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="MaterialGroup", inversedBy="children")
     * @ORM\JoinColumn(name="parent", referencedColumnName="id")
     */
    protected $parent;

    public function __construct() {
        $this->children = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MaterialGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getParent() {
        return $this->parent;
    }

    public function getChildren() {
        return $this->children;
    }

    public function addChild(MaterialGroup $child) {
        $this->children[] = $child;
        $child->setParent($this);
    }

    public function setParent(MaterialGroup $parent) {
        $this->parent = $parent;
    }

    public function removeParent() {
        $this->parent = null;
    }

    public function toArray(){
        $array = (array) $this;
        return $array;
    }
}

